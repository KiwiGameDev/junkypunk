// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHeartWidget.generated.h"

class UImage;

UCLASS(Abstract, Blueprintable, BlueprintType)
class IEPROJ_API UPlayerHeartWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void Show();

	void Hide();

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UTexture2D* FullHeartTexture;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UTexture2D* EmptyHeartTexture;
	
	UPROPERTY(meta = (BindWidget, AllowPrivateAccess = "true"))
	UImage* HeartImage;
};
