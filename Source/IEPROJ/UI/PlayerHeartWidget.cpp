// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerHeartWidget.h"
#include "Components/Image.h"

void UPlayerHeartWidget::Show()
{
	HeartImage->SetBrushFromTexture(FullHeartTexture);
}

void UPlayerHeartWidget::Hide()
{
	HeartImage->SetBrushFromTexture(EmptyHeartTexture);
}
