// Fill out your copyright notice in the Description page of Project Settings.

#include "BobbingMovementComponent.h"

UBobbingMovementComponent::UBobbingMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UBobbingMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	Timer += DeltaTime;
	
	if (ComponentToMove)
	{
		const float Height = FMath::Sin(Timer * Speed) * Amplitude;
		ComponentToMove->SetRelativeLocation(FVector(0.0f, 0.0f, Height));
	}
}

void UBobbingMovementComponent::Inject(USceneComponent* SceneComponent)
{
	ComponentToMove = SceneComponent;
}
