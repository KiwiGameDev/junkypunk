// Fill out your copyright notice in the Description page of Project Settings.

#include "MainGameInstance.h"
#include "MainSaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "DataAssets/ToolUpgrades.h"

void UMainGameInstance::Init()
{
	Super::Init();
	
	LoadGame();
	InitializePlayerTools();
}

void UMainGameInstance::Shutdown()
{
	Super::Shutdown();
	
	SaveGame();
}

void UMainGameInstance::SaveGame()
{
	USaveGame* SaveGame = UGameplayStatics::CreateSaveGameObject(UMainSaveGame::StaticClass());

	if (UMainSaveGame* MainSaveGame = Cast<UMainSaveGame>(SaveGame))
	{
		// Money
		MainSaveGame->Money = Money;

		// Tool Upgrades
		TArray<int> ToolLevels;
		for (int i = 0; i < PlayerToolUpgrades.Num(); i++)
		{
			ToolLevels.Add(PlayerToolUpgrades[i]->CurrentLevel);
		}
		
		UGameplayStatics::SaveGameToSlot(MainSaveGame, MainSaveSlot, 0);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to save game!"));
	}
}

void UMainGameInstance::LoadGame()
{
	USaveGame* SaveGame = UGameplayStatics::LoadGameFromSlot(MainSaveSlot, 0);

	if (UMainSaveGame* MainSaveGame = Cast<UMainSaveGame>(SaveGame))
	{
		// Money
		Money = MainSaveGame->Money;

		// Tool Upgrades
		const TArray<int>& ToolLevels = MainSaveGame->ToolLevels;
		for (int i = 0; i < ToolLevels.Num(); i++)
		{
			PlayerToolUpgrades[i]->CurrentLevel = ToolLevels[i];
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to load game!"));
	}
}

void UMainGameInstance::ClearSavedGame()
{
	UGameplayStatics::DeleteGameInSlot(MainSaveSlot, 0);
}

void UMainGameInstance::InitializePlayerTools()
{
	for (UToolUpgrades* ToolUpgrade : PlayerToolUpgrades)
	{
		ToolUpgrade->InitializeTool();
	}
}
