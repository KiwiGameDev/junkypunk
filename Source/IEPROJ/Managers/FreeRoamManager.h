// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FreeRoamManager.generated.h"

class ALevelManager;
class AThirdPersonCharacter;

UCLASS(Abstract)
class IEPROJ_API AFreeRoamManager : public AActor
{
	GENERATED_BODY()
	
public:	
	AFreeRoamManager();

	UFUNCTION(BlueprintCallable)
	void EnterFreeRoamFromMainMenu();
	
	UFUNCTION(BlueprintCallable)
	void EnterFreeRoamFromArena();
	
	UFUNCTION(BlueprintCallable)
	void ExitFreeRoam();

protected:
	virtual void BeginPlay() override;

private:
	void EnterFreeRoam(const FTransform& SpawnTransform);

	void OnViewBlendCompleted();
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AThirdPersonCharacter> FreeRoamCharacterClass;
	
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AActor* FromMainMenuSpawnPoint;
	
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AActor* FromArenaSpawnPoint;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float CameraBlendTime = 1.0f;

	// Managers
	ALevelManager* LevelManager;
	
	// Player
	APlayerController* PlayerController;
	AThirdPersonCharacter* FreeRoamCharacter;
};
