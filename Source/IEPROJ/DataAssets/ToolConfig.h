// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ToolConfig.generated.h"

UCLASS(Abstract, BlueprintType)
class IEPROJ_API UToolConfig : public UDataAsset
{
	GENERATED_BODY()

public:
	virtual void Copy(UToolConfig* OtherToolConfig);
};
