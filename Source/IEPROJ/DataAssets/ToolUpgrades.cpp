// Fill out your copyright notice in the Description page of Project Settings.

#include "ToolUpgrades.h"
#include "ToolConfig.h"
#include "IEPROJ/MainGameInstance.h"

void UToolUpgrades::InitializeTool()
{
	CurrentToolConfig->Copy(ToolConfigs[CurrentLevel]);
}

bool UToolUpgrades::BuyUpgrade(UGameInstance* GameInstance)
{
	// Check if game instance is MainGameInstance
	UMainGameInstance* MainGameInstance = Cast<UMainGameInstance>(GameInstance);
	if (!MainGameInstance)
	{
		UE_LOG(LogTemp, Error, TEXT("GameInstance is not of type MainGameInstance!"));
		return false;
	}

	// Check if there is next upgrade
	const float NextLevel = CurrentLevel + 1;
	if (ToolConfigs.Num() < NextLevel || Prices.Num() < NextLevel)
	{
		UE_LOG(LogTemp, Warning, TEXT("No more available upgrades!"));
		return false;
	}

	// Check if enough money
	const int Price = Prices[CurrentLevel + 1];
	if (MainGameInstance->Money < Price)
	{
		UE_LOG(LogTemp, Warning,
			TEXT("Not enough money! Current money: %d | Price: %d"),
			MainGameInstance->Money,
			Price);
		return false;
	}

	UE_LOG(LogTemp, Log, TEXT("Upgrading from %d to $d!"), CurrentLevel, NextLevel);

	MainGameInstance->Money -= Price;
	CurrentLevel = NextLevel;
	InitializeTool();

	return true;
}