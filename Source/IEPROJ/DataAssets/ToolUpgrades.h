// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ToolUpgrades.generated.h"

class UToolConfig;

UCLASS(Abstract, BlueprintType)
class IEPROJ_API UToolUpgrades : public UDataAsset
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void InitializeTool();
	
	UFUNCTION(BlueprintCallable)
	bool BuyUpgrade(UGameInstance* GameInstance);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName ToolName;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int CurrentLevel;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UToolConfig* CurrentToolConfig;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<UToolConfig*> ToolConfigs;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<int> Prices;
};
