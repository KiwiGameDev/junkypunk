// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorRuntimeDataAsset.h"

void UActorRuntimeDataAsset::Add(AActor* Actor)
{
	Actors.Add(Actor);
	OnArrayUpdated.Broadcast(Actors.Num());
}

void UActorRuntimeDataAsset::Remove(AActor* Actor)
{
	Actors.Remove(Actor);
	OnArrayUpdated.Broadcast(Actors.Num());
}

void UActorRuntimeDataAsset::DestroyAll()
{
	TArray<AActor*> ActorsCopy = Actors;
	
	for (AActor* Actor : ActorsCopy)
	{
		Actor->Destroy();
	}

	Actors.Empty();
	OnArrayUpdated.Broadcast(0);
}

int UActorRuntimeDataAsset::Num()
{
	return Actors.Num();
}
