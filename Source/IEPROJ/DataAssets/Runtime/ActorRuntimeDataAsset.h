// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ActorRuntimeDataAsset.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnArrayUpdated, int, Size);

UCLASS(BlueprintType)
class IEPROJ_API UActorRuntimeDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	void Add(AActor* Actor);
	
	void Remove(AActor* Actor);
	
	void DestroyAll();

	int Num();
	
	// Delegates
	UPROPERTY(BlueprintAssignable)
	FOnArrayUpdated OnArrayUpdated;
	
private:
	TArray<AActor*> Actors;
};
