// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "NPCRuntimeDataAsset.generated.h"

class ANPC;

UCLASS(BlueprintType)
class IEPROJ_API UNPCRuntimeDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly)
	TArray<ANPC*> NPCs;
};
