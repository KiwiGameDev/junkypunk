// Fill out your copyright notice in the Description page of Project Settings.

#include "NPC_AIController.h"
#include "IEPROJ/AI/BlackboardKeys.h"
#include "IEPROJ/Characters/NPC.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "GameFramework/Character.h"
#include "UObject/ConstructorHelpers.h"

ANPC_AIController::ANPC_AIController()
{
	static ConstructorHelpers::FObjectFinder<UBehaviorTree>Obj
	(
		TEXT("BehaviorTree'/Game/_Project/AI/NPC_BT.NPC_BT'")
	);

	if (Obj.Succeeded())
	{
		BehaviorTree = Obj.Object;
	}
	else
	{
        UE_LOG(LogTemp, Error, TEXT("BehaviorTree asset not found!"))
	}
	
	BehaviorTreeComponent =
		CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	Blackboard =
		CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));

	SetupPerceptionSystem();
}

void ANPC_AIController::SetTarget(AActor* NewTarget)
{
	if (NewTarget != this)
	{
		Blackboard->SetValueAsObject(BB_Keys::Target, NewTarget);
	}
}

void ANPC_AIController::SetupPerceptionSystem()
{
	SetPerceptionComponent(
		*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));
	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConfig"));
	SightConfig->SightRadius = 2000.0f;
	SightConfig->LoseSightRadius = SightConfig->SightRadius + 500.0f;
	SightConfig->PeripheralVisionAngleDegrees = 85.0f;
	SightConfig->SetMaxAge(5.0f);
	SightConfig->AutoSuccessRangeFromLastSeenLocation = 500.0f;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;
	SightConfig->DetectionByAffiliation.bDetectEnemies= true;

	GetPerceptionComponent()->SetDominantSense(*SightConfig->GetSenseImplementation());
	GetPerceptionComponent()->OnTargetPerceptionUpdated
		.AddDynamic(this, &ANPC_AIController::OnTargetPerceptionUpdated);
	GetPerceptionComponent()->ConfigureSense(*SightConfig);
}

UBlackboardComponent* ANPC_AIController::GetBlackboard() const
{
	return Blackboard;
}

void ANPC_AIController::BeginPlay()
{
	Super::BeginPlay();

	RunBehaviorTree(BehaviorTree);
	BehaviorTreeComponent->StartTree(*BehaviorTree);
}

void ANPC_AIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (Blackboard)
	{
		Blackboard->InitializeBlackboard(*BehaviorTree->BlackboardAsset);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Blackboard is null!"));
	}
}

void ANPC_AIController::OnTargetPerceptionUpdated(AActor* PerceptedActor, const FAIStimulus Stimulus)
{
	ANPC* CurrentTarget = Cast<ANPC>(Blackboard->GetValueAsObject(BB_Keys::Target));
	ANPC* PerceptedTarget = Cast<ANPC>(PerceptedActor);

	if (PerceptedTarget && !CurrentTarget)
	{
		Blackboard->SetValueAsObject(
			BB_Keys::Target,
			Stimulus.WasSuccessfullySensed() ? PerceptedActor : nullptr);
	}
}
