// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "MainSaveGame.generated.h"

UCLASS(Blueprintable)
class IEPROJ_API UMainSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	void ClearSaveGame();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Money = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<int> ToolLevels;
};
