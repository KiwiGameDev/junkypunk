// Fill out your copyright notice in the Description page of Project Settings.

#include "FindRandomNavigablePoint.h"
#include "IEPROJ/Controllers/NPC_AIController.h"
#include "NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"

UFindRandomNavigablePoint::UFindRandomNavigablePoint()
{
	NodeName = FString("Find Random Navigable Point");
}

EBTNodeResult::Type UFindRandomNavigablePoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const ANPC_AIController* const AIController = Cast<ANPC_AIController>(OwnerComp.GetAIOwner());
	UBlackboardComponent* const Blackboard = OwnerComp.GetBlackboardComponent();
	
	if (UNavigationSystemV1* const NavigationSystem = UNavigationSystemV1::GetCurrent(GetWorld()))
	{
		const APawn* const NPC = AIController->GetPawn();
		const FVector Origin = NPC->GetActorLocation();
		FNavLocation Location;

		if (NavigationSystem->GetRandomPointInNavigableRadius(Origin, SearchRadius, Location, nullptr))
		{
			Blackboard->SetValueAsVector(BlackboardKey.SelectedKeyName, Location.Location);
			FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
			return EBTNodeResult::Succeeded;
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Could not find navigable point!"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not get navigation system!"));
	}
	
	FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
	return EBTNodeResult::Failed;
}
