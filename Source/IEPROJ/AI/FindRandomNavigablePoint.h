// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "FindRandomNavigablePoint.generated.h"

/**
 * 
 */
UCLASS()
class IEPROJ_API UFindRandomNavigablePoint : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:
	UFindRandomNavigablePoint();

protected:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float SearchRadius = 250.0f;
};
