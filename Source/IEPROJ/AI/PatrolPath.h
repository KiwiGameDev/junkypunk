// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PatrolPath.generated.h"

UCLASS(Blueprintable)
class IEPROJ_API APatrolPath : public AActor
{
	GENERATED_BODY()
	
public:	
	APatrolPath();

	FVector GetPatrolPointLocation(const int Index) const;
	FVector GetRandomPatrolPointLocation() const;
	int Num() const;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (MakeEditWidget = "true", AllowPrivateAccess = "true"))
	TArray<FVector> PatrolPoints;
};
