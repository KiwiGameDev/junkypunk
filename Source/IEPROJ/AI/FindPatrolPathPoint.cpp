// Fill out your copyright notice in the Description page of Project Settings.


#include "FindPatrolPathPoint.h"
#include "IEPROJ/Controllers/NPC_AIController.h"
#include "IEPROJ/Characters/NPC.h"
#include "PatrolPath.h"
#include "BlackboardKeys.h"
#include "BehaviorTree/BlackboardComponent.h"

UFindPatrolPathPoint::UFindPatrolPathPoint()
{
	NodeName = TEXT("Find Patrol Path Point");
}

EBTNodeResult::Type UFindPatrolPathPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ANPC_AIController* const Controller = Cast<ANPC_AIController>(OwnerComp.GetAIOwner());
	UBlackboardComponent* const Blackboard = Controller->GetBlackboard();

	if (ANPC* const NPC = Cast<ANPC>(Controller->GetCharacter()))
	{
		if (APatrolPath* const PatrolPath = NPC->GetPatrolPath())
		{
			FVector PatrolLoc = PatrolPath->GetRandomPatrolPointLocation();
			Blackboard->SetValueAsVector(BB_Keys::TargetVector, PatrolLoc);
		}
		else
		{
			FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
			return EBTNodeResult::Failed;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to cast character to NPC!"));
		FinishLatentAbort(OwnerComp);
		return EBTNodeResult::Aborted;
	}

	FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	return EBTNodeResult::Succeeded;
}
